<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About this project

This project is a test (Tickets) for Double V Partners (RESTful API).

### Steps for deploy this project in development environment

This project needs a database (mysql). Remember to configure the .env file in the root directory with your database accesses.

Execute the following commands after configuring the database (.env file):

1. composer update
2. php artisan config:clear
1. php artisan migrate:fresh --seed
1. php artisan serve
2. php artisan test

_You can now access http://localhost:8000 and view the project. Please read the documentation for handle endpoints._

### [API Documentation](https://documenter.getpostman.com/view/3719930/2s93sgYBMz)
