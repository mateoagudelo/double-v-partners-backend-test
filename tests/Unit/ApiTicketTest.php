<?php

namespace Tests\Unit;

use Tests\TestCase;

class ApiTicketTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_list_tickets(): void
    {
        $response = $this->get(route('ticket.index'));
        $response->assertStatus(200);
    }

    public function test_create_ticket(): void
    {
        $response = $this->post(route('ticket.store'), [
            'subject' => 'I need help with my new product',
            'description' => 'My product not working in my house',
            'user_id' => 1,
            'status' => 'open'
        ]);

        $response->assertSuccessful();
    }
}
