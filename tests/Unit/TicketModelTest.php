<?php

namespace Tests\Unit;

use App\Models\{User, Ticket};
use Tests\TestCase;

class TicketModelTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    private $ticket;

    public function setUp(): void
    {
        parent::setUp();
        $this->ticket = Ticket::factory()->make();
    }

    public function test_ticket_belongsto_user(): void
    {
        $this->assertInstanceOf(User::class, $this->ticket->user);
    }

}
