<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use App\Models\User;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->make();
    }

    public function test_user_has_many_tickets(): void
    {
        $this->assertInstanceOf(Collection::class, $this->user->tickets);
    }

}
