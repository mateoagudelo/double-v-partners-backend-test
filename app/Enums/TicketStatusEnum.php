<?php

namespace App\Enums;

enum TicketStatusEnum:string
{
    case PENDING = 'pending';
    case OPEN = 'open';
    case CLOSED = 'closed';
}
