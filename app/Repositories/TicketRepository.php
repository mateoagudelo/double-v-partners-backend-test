<?php

namespace App\Repositories;

use App\Models\Ticket;

class TicketRepository implements TicketRepositoryInterface
{
    protected $model;

    public function __construct(Ticket $ticket)
    {
        $this->model = $ticket;
    }

    public function all()
    {
        // TODO: Implement all() method.
        return $this->model->paginate();
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
        $ticket = $this->model->create($data);
        $ticket->load('user');
        return $ticket->fresh();
    }

    public function find(int $id)
    {
        // TODO: Implement find() method.
        return $this->model->findOrFail($id);
    }

    public function update(array $data, int $id)
    {
        // TODO: Implement update() method.
        $ticket = $this->model->whereId($id)->firstOrFail();
        $ticket->update($data);
        return $ticket->fresh();
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
        $ticket = $this->model->whereId($id)->firstOrFail();
        $ticket->delete();
        return $ticket;
    }
}
