<?php

namespace App\Models;

use App\Enums\TicketStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    /**
     * We allow this @guarded attribute to validate all attributes later.
     *
     */
    protected $guarded = [];

    /**
     * Append user relation with all queries
     *
     */
    protected $with = ['user'];

    /**
     * The status attribute is casted by @TicketStatusEnum
     *
     */
    protected $casts = [
        'status' => TicketStatusEnum::class
    ];

    /**
     * One ticket belong to one user.
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
