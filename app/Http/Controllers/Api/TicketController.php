<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\TicketRepositoryInterface;
use App\Http\Resources\Api\TicketResource;
use App\Http\Requests\Api\TicketRequest;

class TicketController extends Controller
{
    /** @var TicketRepositoryInterface */
    private $repository;

    /** @var ticket */
    private $ticket;

    /**
     * Display a listing of the resource.
     */
    public function __construct(TicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return TicketResource::collection($this->repository->all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TicketRequest $request)
    {
        //
        $this->ticket = $this->repository->create($request->validated());
        return new TicketResource($this->ticket);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        //
        return new TicketResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TicketRequest $request, int $id)
    {
        //
        $this->ticket = $this->repository->update($request->validated(), $id);
        return new TicketResource($this->ticket);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        //
        $this->ticket = $this->repository->delete($id);
        return new TicketResource($this->ticket);
    }
}
