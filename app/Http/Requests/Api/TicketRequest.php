<?php

namespace App\Http\Requests\Api;

use App\Enums\TicketStatusEnum;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rules\Enum;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        if(request()->isMethod('PUT') || request()->isMethod('PATCH')) {
            return [
                'user_id' => 'numeric|exists:users,id',
                'subject' => 'string|max:255|min:6',
                'description' => 'string|max:500|min:10',
                'status' => [new Enum(TicketStatusEnum::class)]
            ];
        }

        return [
            'user_id' => 'numeric|required|exists:users,id',
            'subject' => 'string|required|max:255|min:6',
            'description' => 'string|required|max:500|min:10',
            'status' => [new Enum(TicketStatusEnum::class)]
        ];

    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ], 400));
    }
}
