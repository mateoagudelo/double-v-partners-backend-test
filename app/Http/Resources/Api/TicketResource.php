<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'status' => @$this->status->value,
            'created_at' => $this->created_at->diffForHumans(),
            'latest_updated' => $this->updated_at->diffForHumans(),
            'user' => new UserResource($this->whenLoaded('user'))
        ];
    }
}
